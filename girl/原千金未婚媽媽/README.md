# novel

- title: 元貴族令嬢で未婚の母ですが、娘たちが可愛すぎて冒険者業も苦になりません
- title_zh1: 原千金兼未婚媽媽、因為女兒們很可愛所以當冒險者完全不覺得辛苦
- title_en1: Even Though I'm a Former Noble and a Single Mother, My Daughters are too Cute and Working as an Adventurer Isn't Too Much of a Hassle
- author: 大小判
- illust:  まろ
- source: http://ncode.syosetu.com/n2129ei/
- cover: https://images-na.ssl-images-amazon.com/images/I/91v9n0PqG6L.jpg
- publisher: syosetu
- date: 2019-03-12T02:40:00+08:00
- status: 連載
- novel_status: 0x0100

## illusts


## publishers

- syosetu

## series

- name: ざまぁシリーズ

## preface


```
　２０１９年、３月９日。ＴＯブックス様より第２巻発売が決定しました！　詳しくはイラストが記載された活動報告をどうぞ！
　第二章、帝国ざまぁ編始めました！
　白髪とオッドアイを蛇蝎の如く嫌う帝国貴族、その最高位である公爵家に生まれたシャーリィは、見事な白髪と、紅色と蒼色のオッドアイを持って生まれてきた。
　血の繋がった家族からも疎まれ、巡り会った自分を愛してくれる婚約者も実の妹に奪われ、冤罪を着せられた上に投獄されてしまう。
　激しい拷問の末、自分を貶めた全てに復讐を誓い、運良く脱獄に成功したのだが、彼女のお腹には既に元婚約者との間に出来た子供が宿っていた。
　例え憎い男の血を引いていたとしても、生まれてくる子供に罪はない。せめて無事に生んで、後は孤児院にでも預けようとしたのだが――――

「どうしましょう、私の娘たちが世界一可愛いんですけど」

　お腹を痛めて生んだ我が子の可愛さにノックアウトされたシャーリィ。復讐よりも子育てを選び、帝国を飛び出して隣の王国の辺境の街に移り住んだ彼女は養育費や生活費を稼ぐために冒険者ギルドの扉を開く。目立たず、ひっそりと、帝国に気付かれないように暮らせればいいと思っていたが――――

「シャーリィさん！　王都の近くに天災級の魔物が現れたんですけど、依頼を引き受けてくれませんか！？」
「断ります。今日は娘たちの学校の宿題を手伝う約束をしていますので」

　十年後、何時の間にか世界トップレベルの冒険者、《白の剣鬼》と呼ばれるようになってしまった。


　主人公復讐を止めてますが、後に盛大なざまあが有ります。
　日間ランキング、早くも１位達成！　皆さまの評価、本当にありがとうございます！
```

## tags

- node-novel
- R15
- syosetu
- ざまぁ
- すれ違い
- オリジナル戦記
- クール＆ツンデレ
- ハイファンタジー
- ファンタジー
- 不老
- 冒険者
- 婚約破棄
- 年の差
- 広義的にはロリババア
- 恋愛
- 悪役令嬢
- 残酷な描写あり
- 異世界召喚
- 親バカ
- 親子
- 家族
- 女主人公
- 冤罪
- 復讐
- 女兒
- 異色瞳
-

# contribute

- nyancats2012
-

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id: s8227d
- novel_id: n2129ei

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n2129ei&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [ざまぁシリーズ](http://ncode.syosetu.com/s8227d/)
- [masiro.moe](https://masiro.moe/forum.php?mod=forumdisplay&fid=130&page=1)
- https://www.novelupdates.com/series/even-though-im-a-former-noble-and-a-single-mother-my-daughters-are-too-cute-and-working-as-an-adventurer-isnt-too-much-of-a-hassle/
-


