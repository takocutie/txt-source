
（下面是剑之贵族的宗主的视角）

这个世界上唯一的人类国家、王国。
在受命守护其北边领土的贵族的宅院裡。
居住着以剑为生，以持剑为义务的人们。
半夜裡，大多的人都睡着了的时候，书房裡响起了砰砰的敲门声。

「进来。」
「失礼了。」

与我的入室许可同时，静静走进来的是管家塞巴斯汀。

「發生了什麼事？」

我不特意将意识转移，只是在蜡烛的光线下阅览着扎堆的请愿书，并答道。

「那是……」

我家能幹的总管家，十分稀奇的吞吞吐吐的说道。
平时也许会因在意而质问他，现在不行，我在工作中，无暇顾及到他。

「没看见我现在很忙吗？简明扼要地说，如果是无关紧要的話…「阿尔特大人」……呼…怎麼了？」

不是“宗主”而是“阿尔特大人”吗。
赛巴斯特地直呼我的名字时，大多有重要的内容要说。我勉强抬起头，尽管不情愿还是摆出了听的姿势。
是长时间在战场上培养出来的经验吗。
我觉得有不好的预感。

「阿尔特大人，虽然这件事很难说出口……」

面对赛巴斯暧昧不清的态度，我无言地催促着。

「……小姐，伊丽丝大人，已经到极限了。」
「……？」

意想不到的言语，让我感到困惑。
……本想着他会说些什麼……看来他也老糊涂了吗……
不好的预感没有中，让我感到失望，惊讶的我反问道。

「说些什麼呢赛巴斯。你也知道的吧？将肉体和精神逼到极限，在超越极限的时候才能…「阿尔特大人」…哈、你到底想说什麼？」

赛巴斯再次打断我的話语。
我多少有些生气，但还是让他继续说下去。

「……恕我直言，小姐已经没有觉醒闘气的可能性了……」
「……哈？」

对於不成熟的长女的可能性，赛巴斯应该比亲生父亲的我更加相信着才对。
刚开始，我没有理解这句話的意思。
然後在理解的同时，心中涌起了怒火。

「……继续说。」

赛巴斯平淡的说出他的根據。

「小姐她已经不在看这个世界了。」
「？」
「小姐与她空想出来的人物结为朋友，并躲进了自己的世界中。虽然不清楚这情况是从何时开始的，但是继续进行小姐的修行恐怕也是无意义的。对於小姐而言都是另一个世界的事情。恐怕就连痛苦也会被感觉成空虚吧。」
「……」

因悲伤而颤抖的声音。
赛巴斯的眼瞳中没有一丝谎言。

「……这是…这就是常年侍奉我家的，你的看法吗？」
「听从您的意见。小姐她已经没有生活在这个世界了。这以上的训练对宗主，对小姐都只会感到痛苦而已。」
「……我明白了。但是，你在清楚了缺乏力量的贵族的末路之後，还是这麼说吗？」
「……」

赛巴斯对我的质问默默地点头。
……我的女兒也终於被赛巴斯抛弃了吗……
我心中是有数的。在训练时总是心不在焉。被砍到的时候的反应，也确实变得迟钝了。

「……是吗……。但是，到七岁为止。到了七岁如果她还没能觉醒闘气，作为剑之贵族的宗主，作为一名父亲，我就要做出相应的處置了。」
「……是。真是遗憾……」
「别说了。这就是无力之人的宿命。我们贵族，不得不比他人更加强大才行。严律守己、锻炼自己、站在最前线战鬥是我们的命运。」

……伊丽丝……
脑海中映出女兒哭泣的面容。
……你恨着我吗……？
这时剑之贵族的宗主，阿尔特的心情是複杂的。
仅仅为了女兒的未来，狠下心来，封闭了自己的感情，他的辛苦并没有得到回报。全部都是徒劳的。

「阿尔特大人特地扮演鞭子角色……要是我早点注意到的話，就会有另外的方法……」
「……赛巴斯，抱歉让我一个人静静好吗？」
「……是。失礼了。」

赛巴斯神情悲伤的行礼後，退出了房间。
房间裡只身一人，剑之贵族的宗主，阿尔特连阅览请愿书的气力都消失了。

「作为父亲之前，我是一名贵族……对不起，伊丽丝，请原谅我……」

将爱和义务放在天平上，他选择了义务。
但是，无论选择了那一边都会留下遗憾吧。

「对不起……」

对自己的想法而捨去的事物的谢罪，希望被原谅，这仅仅是自我满足罢了。
依视角不同，这或许是及其傲慢的。
因为比任何人都严厲的他，也许比任何人更加深爱着自己的女兒……

==================分割线==================

（下面是克莉丝的视角）

「……那、那个…这是……」

我的嘴巴，空虚地重複着一张一合的动作。

「天使小姐，到底發生了什麼？」

场所是地牢。在我眼前的是纯白色的，如同男孩般的少女。飘荡着铁锈味的牢房中，我们面对面的坐着。

「……那是……」

……都到这裡了，像在家裡练习的那样……在床上练习的那样……

「……」

我的背上冷汗直流。
……与床上练习时候的紧张感完全不同……！

「……啊、啊……」

……阿勒？我要说…应，应该说些什麼才好……
极度紧张的我头脑中变得一片空白。想要说的話如同迷路的孩子一般，不知去了何处。

「……？嗯~对了！天使小姐，不介意的話进行自我介绍吧。那个呢，我的名字是伊丽丝。伊丽丝·诺尔·伊斯兰菲尔。天使小姐的名字呢？」

对我也能开朗的进行对話的女孩子，伊丽丝。
明明有着辛酸经歷的她，将负感情遗忘在何处似的，微笑着进行自我介绍。
在我面前一直微笑着。

「……诶……」

内心受到了冲击的我，断断续续地说出了自己的名字。

「……那、那个……克、克莉丝…伊斯特·阿、阿兹…瑞尔……」
「克莉丝…嗯，天使小姐是克莉丝酱呢！咦？但是，阿兹瑞尔是……诶~多，难，难道是莱尔君的妹妹吗？」

酱？……好羞耻……
但是，伊丽丝认识莱尔呀。
看见点点头默认的我，伊丽丝开心的笑着。
但是，笑容中泛着一丝阴影。
这一瞬间浮现出的阴影，大概是她和我之间不起眼的，又巨大的接点。

「这样啊。谢谢你一直以来治疗我的伤口。但是，天使小姐是熟人的妹妹让我很吃惊呀。」
「……啊……！」

不知为何伊丽丝稍微放鬆了下来。
但是，她的話让我想起了最初的目的。
……对了，我是为了成为这个孩子的朋友才过来的……！

「……那、那个……」
「嗯？有什麼事？」

感到不可思议的是，注视我的伊丽丝的视线，让我感到害羞。
……没，没关係的……就像对着墙壁说話，就像对着枕头说話……在、在精神年龄上我可是年长好多岁……
羞耻与期待，以及要将我压垮的不安。
凝视着牢房裡沾着污垢的石壁，我鼓起勇气。
将覆盖身体的魔装增加了一层出力。

「……我、我…虽然…肮髒……」
「诶？克莉丝酱…在说什麼……？」
「……虽然肮髒…但，但是…成为…朋友……」

途中伊丽丝好像说了些什麼，但是充耳不闻的我一口气将話语说完，低着头等待着回答。

「……」

会被阻止吗？
会被拒绝吗？
还是说因为太讨厌了，而让我不要靠近吗？
脑袋变得好热，体感时间变得缓慢，激烈的心跳停不下来。

「「……」」

长时间的沉默。
再也忍不住的我，即将消除身影离去的时候，终於听到了轻轻的偷笑声。
我放鬆下来，睁开眼睛，看到的是伊丽丝在微笑着。

「……纯洁美丽的克莉丝酱，真的希望和我成为朋友吗？」
「……诶……？」

看着战战兢兢的我，她自嘲地说道。

「但是呢，克莉丝酱。我连闘气都无法使用，明明是贵族却这麼弱小，头脑也不好使。我和克莉丝酱的哥哥，莱尔君完全不一样的哟？我和克莉丝酱一点都不般配哟？估计你很快就会开始讨厌我的哟？」

伊丽丝的红瞳，在说話途中渐渐湿润。
她辛酸地笑着，心情却是喜悦的，还对我说了“谢谢”。

「……诶……」

但是最後的言语，已经听不到了。
……我被…拒绝了吗……？

「……不行……？」

脑海中不断回旋着“拒绝”二字。
……我、我早就明白了……果然像我这种辣鸡……
……但是，稍微，真的是稍微期待了一点点……
反动让泪水从我的眼眶中滴落。
但是，这种害羞的事情，我低下头不让她看到。

「……哭……」

思考着境遇相似的我们，也许能成为好朋友……
期待着她在知晓我之後，依然能成为好朋友……

「……我…回家…「但是，如果克莉丝酱」…了……？」

手上触碰到的温暖的感觉。
对听到的声音，我再次抬起头，这次是稍微感到不安的伊丽丝，靠近过来的面容。
过於惨白的脸颊，在心理作用下染上朱红。

「如果，不对，诶~多…克莉丝酱觉得我，觉得这样的我也可以的話……不嫌弃的話……」
「……」
「和我成为熟人……不对，能和我成为朋友吗……？」
「……诶……？」

倾斜着小脑袋、流着泪水的，可爱的女孩子。
看着她让我停止思考。心臟的悸动变得强烈起来。

「——」

伊丽丝害羞地说了些什麼，但是我停止工作的大脑却理解不了。
——朋友——
是多麼甜美的声音呀。

「……朋…友……」

我口中不断的重複着。

「……朋、朋友……」

脸上自然的露出了笑容。
我尽全力保持着僵硬地笑容，看着伊丽丝。
但是，在面前坐着的伊丽丝，不知为何开始慌张起来。

「克、克莉丝酱。眼泪！眼泪流出来了！」

手指拂过脸颊，我终於意识到自己在哭泣。
……啊、好羞耻……

「你看，诶~多，打起精神！」

即使是这样的我也温柔对待着的白色少女，伊丽丝。

「……真、真的…可以吗……？」

对於我断断续续的确认的言语，伊丽丝也没有露出厌恶的神色，点头肯定着说道。

「嗯！嗯！！我才是，今後也请多多指教！克莉丝酱！」
「……多、多指教……！」

算上前世，也是我的第一个朋友，她是一个不满7岁，白髮红瞳的，不可思议的，男孩子般的少女……

=====================分割线======================

「有克莉丝酱陪伴，我太高兴了。」

有时对生活感到艰辛而大声哭泣的伊丽丝，最後一定会微笑着这麼说道。

「……嗯……」

每当听到这句話、看到她的笑容，我的心就会激烈跳动。
稍微有些痛苦，但是却不觉得讨厌。
有时，我的心中会浮现出“我如此幸福的生活是被允许的吗？”的疑问……

「……我也…高兴……」

也许这就是朋友吧。

「「……」」

紧紧握住的受伤传来的体温，和小孩一样，温暖、舒适。

「……因为我们不只是一个人。」

微笑着的小孩子，精神年龄上远低于自己的，幼小的孩子。
与过去的自己相似的，但是，又完全不同的朋友。非常、非常重要的人。
只有在地牢中才能相见，一方的心中有着孤独的、无法治癒的伤口，另一方总是被别人虐待、欺凌……

「……嗯……」

但是，两人之间确实有羁绊相连。

「谢谢你，克莉丝酱……真的，永远在一起就好了……」

两人在一起的时候先不会交流。
但是克莉丝非常喜欢这段时间，将其看得十分重要。
克莉丝的身边有伊丽丝在，伊丽丝的身边也有克莉丝在。
得到了理解者之後，她们的世界裡第一次变得色彩鲜明，变得明亮起来……

