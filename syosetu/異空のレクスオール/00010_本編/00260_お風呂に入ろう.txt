
被艾丽莎目送着，要走进了公共浴场中。
总之先採取跟着其他男性这种随大流的消极战术，总算避免了“误入女澡堂”这种约定俗成的事故，要成功来到了收费的地方。

“那个，我想进去”
“铜币3枚。要购入洗澡布和肥皂的话需要另外支付2枚银币。”

想着的确刚才没有买到肥皂，要拿下挂在脖子上的皮袋向裡面看去，仔细确认後拿出2枚银色硬币和3枚铜色硬币递给接待员。

“好的，全部使用王国货币支付是吧。那麼肥皂和洗澡布在这裡。这裡也提供浴巾的租借，如果您需要使用的话请再支付一枚铜币……”

一开始就先说啊，要心裡抱怨着将一枚铜币放在柜台上，结果了递过来的物品。
肥皂的话，放在皮袋裡的那个还有那麼回事，但洗澡布和浴巾与其说是毛巾，基本就是真真正正的“布”而已。
（注：我姑且以、洗い布—洗澡布、身体拭き—浴巾、这样翻译，这两个词我是真没查准，基本是根據上下文翻的）
在这种地方就不需要体现异世界的差异了啊……要边这样想边準备走进裡面……这时柜台的女接待员“啊”地发出声。

“啊—……差了什麼吗？”
“没有，请慢走”
“谢谢”

她到底是想叫住要呢，还是觉得有义务要这麼做呢。（原文：それは呼び止めることだったのだろうか。それとも義務付けされているのだろうか。这句话真没看懂）
抱着这样无所谓的疑问要走进内部……到达了一个有着大量明显是店员的少年们的房间。
可以看到正在脱衣服的估计是客人的男人，这裡应该就是更衣室了，不过既没有筐子也没有架子。
要正烦恼着该怎麼办时，正好看到其中一个少年走到刚才的男子旁边接过了衣服。
同时也注意到男子递过去了几枚应该是铜币的硬币，可能是小费或保管费……嘛，大概就是这种东西吧。
既然这样，要也适当地走到墙边开始脱衣服……但来自背後的视线总让人觉得很不自在。
不知道是不是为了争夺保管权，甚至能感受到互相牵制的气氛。
正因为视线都来自于少年所以还能接受，如果这都是成年男性的视线的话，要肯定就全力地逃跑了。
总算脱下衣服缠上洗澡布时，一位少年像是瞄准着这个时机在右侧出现。

“这位客人，请由我来保管您的衣服和浴巾吧”
“欸、啊、好的。帮大忙了。这些够吧”

说着，要从皮袋中取出大概五枚铜币递了过去，少年微笑着回答道“请慢用。直到客人您出来之前，我会负责地保管这些东西的”。
看来做的没错而安心的要正準备进到更裡面时……少年“能再打扰一下吗”地搭话道。

“欸？好的”
“借一步说话”（お耳を拝借）

要稍稍屈膝弯下腰，少年在要耳边轻轻地“莫非，您是贵族大人或有名的商人吗？”问道。
当然要的回答是“只是普通的庶民啊”、但少年却一副理解了的表情“是这样吗”地点着头。

“要说的就是刚才那些吗？”
“是的。不过，也是由於您的举止才会这样想的。在这种地方也是有着寻找猎物的坏人的，您在归途时还请多多在意”

……原来如此，既然共同浴场是谁都可以利用的场所，所以也会有富裕阶层会过来吗。
所以也有人会特意去在回去的路上袭击这些富裕阶层……看来是被这样忠告了。

“……谢谢你，我会注意的”
“没有的事。要说的话，客人您如果还有什麼忘了需要寄存在我这裡的东西的话，还请趁现在”

注意到这委婉的追加保管请求，要又递过去2枚铜币，少年“非常感谢，这位客人”展示出夸张的反应。
但也只是为了表现自己没有刻意要求的表演吧。
从其他少年们羡慕的表情来看，恐怕要被当成贵族或商人这样“富裕的人”这点不假。
实际上别说富裕，就连这小小的钱包也是艾丽莎给的……但要也没準备把话说得那麼清。

“真是不得了啊”

要苦笑着离开更衣室，走进有着澡堂的房间。
可能因为还没到晚上所以人还不算多，宽广的澡堂裡只有幾名客人。
虽然有着像是冲洗场的地方，不过当然不可能会有淋浴喷头所以要也完全不知道怎麼用。
既然有洗澡布和肥皂，那肯定会有用这些东西的取水处才对，但对第一次过来的要来说，肯定是不可能知道的。
虽然这麼说，但去问其他客人的话，肯定会被“这是有多不知常识”地吸引目光，这点是需要避免的。
不洗身体直接进浴池肯定也是不行的，所以虽然不想去看，但也只能去观察其他人是怎麼洗的了。

“……早知道就该先问清楚怎麼进来的啊”

又不敢做出什麼太失礼的行为，要只能叹着气向远处望去。
