

返回古兰歇斯领的数日後。
虽然利铎尔希姆的祭典让我享受到相当的乐趣，但果然对於拥有前世记忆的我们来说总觉得欠缺了什麼――主要是在甜味的意义上。
简单来说，就是想吃蛋糕之类的食物了。
虽说叫爱丽丝做给我就好……但听闻穆勒之街开了一家新的店，所以我决定和爱丽丝往穆勒之街一探究竟。
「想不到，这条街道也完全发展起来了呐」
我一边眺望着街道一边深有感触的嘟哝着。
两侧林立着的是伪装成红砖砌成的建筑物――而实际上是使用钢筋混凝土建成的商店。有咖啡店，杂货店等。除此之外还有各式各样的店建成一列。
此外中间的大道是以砖块整齐铺满的主要街道，为了降低發生事故的风险，在马路左右两侧设置着行人道。
然後在那样的行人道走着的，大部份是穿着哥德式制服的学生们，其次就是穿着洋服，不认真看会以为是日本人的人们。
「……说起来，是在什麼时候量产了那些衣服的？ 丝绸和羊毛绒之类不是还不能量产的吗？」
「那是用木棉和麻布之类制作的廉价的量产品呦」
「噢…什麼时候…」
倒不如说，那些廉价量产品的主要素材是麻？虽说在这个世界是挺便宜的，但作为天然素材来说可是相当高级的吧。
…之类的边想着这样的事边望向旁边，本应一起步行的爱丽丝不知什麼时候不见了。咦…急忙回头寻找，发现爱丽丝停住脚步凝视着某个地方。
「……爱丽丝？」
想着發生了什麼事而投向视线，在那看到的是……露天咖啡店？不，是在露天咖啡店聊天的学生们…吗？
「啊，抱歉」
注意到我视线的爱丽丝以小碎步跑过来。
「怎麼了吗？」
「唔，什麼事都没有呦。比起那个，今天要去哪裡的店？」
被蒙混过去……了吗？ 嘛…无所谓吧？总觉得预想到原因了，现在就先不追究吧，我边这样想着边转换了心情。
「关於那个目的地啊，我从克蕾雅姐那听说了有家能洗足浴的咖啡店开张了哦」
「利昂还真是喜欢足浴呢」
「在冬天一边泡脚一边吃着蛋糕，就像在被炉里吃蜜柑一样，不觉得那样很享受吗？」
虽然很遗憾在这个世界上没有被炉，但我觉得洗足浴（的舒服程度）能匹敌（被炉）了。
「在人前脱去袜子和鞋泡在热水裡，可是相当让人害羞的呦？」
「是吗？大家不是一副不在乎的样子（泡）进去的吗？」
想起了那时在宅邸一边泡脚一边工作的时候。克蕾雅姐和索菲亚，还有其他的大家也，常常聊天时顺便去泡脚。
「因为在这个世界，甚至残留着男女一起入浴的风俗呢。虽说贵族到底好像是没有那样的（风俗），尽管如此我觉大家都是对此不怎麼在意」
「哼姆。那就是说觉得害羞的是爱丽丝吗……」
如果是那样的话还有机会一边洗足浴一边嚐甜品吗。（也就是说妹子不想去的话，男方基本上都去不了）
足浴咖啡店，真是想去呐。怀着觉得遗憾的视线而偷盯着爱丽丝时，她像是受不了地叹息了一下。
「……没办法呐。只有今次是特例哦，（平时）要是有其他男人在才不会泡的呢？」（傲娇语气）
这样说着而稍微红着脸的爱丽丝真可爱。
被说了，只有我是特别的才会忍耐着（害羞也要做），总感觉这很让人心动啊。我为了掩饰那样的心情而先开始往前走着。
然後就来到了足浴咖啡店。不知为何店员是以女仆的装扮说着，『欢迎回来，主人，大小姐』的出来迎接了。
原来是女仆咖啡店吗！
的这样想着，看来真的好像是足浴女仆咖啡店。
顺便一提，每各餐桌都有简单的屏风区隔着，造成小型独立单间的感觉。而在那样的单间中泡着脚的爱丽丝则完全放鬆了下来。
在来到之前，明明是那麼抗拒的呐
「哈呼……泡脚真舒服呢……」
「之前说的害羞跑哪去啦？」
「嗯~? 即使现在也觉得有点害羞呦？」
「哪有啊」
「因为，泡脚也算是泡澡吧？」
「虽然也没错，但有好好地穿着衣服的吧？」
虽然男人们需要卷起裤脚，但爱丽丝因为穿着超短裙的原因所以只需脱去着鞋和高统袜。明明并不是什麼会害羞的装扮啊。
「即使穿着衣服，放鬆整个人就跟毫无防备是同样意思的关係，感觉上来说不就像是混浴那样吗。所以，果然还是有点害羞呦」
「姆姆……」
被这样说的话，总觉得跟人一起泡脚就像做什麼背德的事情呐。说起来，因为泡脚令整个人暖和起来了吗，脸颊稍微变得绯红的爱丽丝的身姿总感觉相当妖艳。
而且，仿如脱力般趴着的丰满胸部往桌子挤压着，每当爱丽丝动的时候就会改变其形状。
……感觉反而是看着的这边害羞起来了啊。
「但是呢。如果看着这样（忍着害羞）的我能让利昂心动起来的话，我觉得这样也可以啦」
――我说，被看穿了了了了了了！? 可恶，我又被爱丽丝玩弄於股掌之间了吗……总有点不甘心。
「呼呼，利昂也终於到了（对“性”感兴趣的）年纪了呢」
呜，到底要怎回答才好啊。全靠第二性徵发育完成的关係，变得会各方面在意女性的事…之类的？不可能说的出口。
顺带一提，（第二性徵发育完才）在意女性只是米纸做包（表面/靠不住脚）的说法。从精神上来说，自转生起就已经有意识到女性的想法。
只是以前就好像一直处在贤者模式感觉下的关係，所以在第二性徵发育完成後才开始对女性的想像各种奔腾……大概那样的感觉啦。
或许，差不多是时候要对爱丽丝她们的感情作出结论了呐――在那样的方向思考下，突然传来了耳熟的声音。
「喂~啊，爱莎。不要太嬉闹了呦。会给其他的客人带来困扰的吧」
「但是前辈，女仆咖啡店呦？ 女仆桑给我伺候呦？ 我们这种一般市民本应过上一辈子也不可能会体验到的（伺候）呦！?」
「唔……但是…我，偶尔也会被传唤到利昂大人的宅邸啊」
「Ku~真令人羡慕。老是前辈（捞到好处）太狡猾了啦」
虽然不记得有听过那叫爱莎的女孩子的声音，但被称作前辈的女孩子的声音则有印象在哪听过。
我从座位的屏风上露出脸往声音的方向看去。在那裡，有正如我预想的丽安娜的身影在。
「吔呵，丽安娜。在这样的地方还真是奇遇呐」
自丽安娜作为二期生代表从学校毕业之後，就一直做着穆勒学园的教师。
今年是十七岁。虽然入学初已显露出她那可爱的片隅，但成长後更增添了那份可爱。
因此，被入学的男学生们一个劲地告白而疲於应付着的样子――以上，是来自克蕾雅姐的情报。
「咦，好久不见。（什麼时候）返回穆勒之街了的」
「就数日前呐。丽安娜还精神吗？」
「嗯，我很好啊」
「――前辈前~辈，这个说话臭屁的男孩子是谁啊？」
那叫爱莎来着的女孩子，砰砰地拍打着我的头。在那瞬间，丽安娜的脸失去了血色。但是爱莎并没有注意到，往我的脸窥视着。
那样的爱莎是……比丽安娜稍为年轻吗？是在这边比较少见的绿色头发配上黑色的瞳孔。看起来很活泼的短发女孩子。
既然称呼着丽安娜为前辈，也就是说是第三期毕业生的新人教师吧。尽管如此不知道我的原因，大概是我最近没怎麼在学校出现的关係。
姑且，是有出席过三期生的毕业纪念聚会啦，我想应该会有留下擦肩而过程度的印象呐。
「哼姆哼姆。脸蛋姑且是合格呢。但是呢少年。虽然我明白（你）憧憬着丽安娜前辈（的心情），但再怎麼装出大人的气氛，也是射不穿前辈的心的呦？」
姆姆。在我思考的时候，还真是被随便地说了（喜欢丽安娜）啊。
或者说，爱莎要是丽安娜的後辈的话，大不了就是稍後比我年长一点点。虽然我想年龄应该是差不了多少的呐。
「为什麼沉默了呢？因为被说中了所以无法反驳了吗？うりうり」（戳脸拟声词）
快住手别戳脸颊――虽然想当场说出来，但至今为止周围没有一个人对我作出过这样的反应，这感觉挺新鲜的并不坏。
「爱爱爱莎！不行啊，给我住手！ 那位大人是――」
「……这孩子怎麼了吗？」
正当丽安娜打算表明我的身份…的咫尺之前将说话咽下。
原因是趁着爱莎回头看向丽安娜的间隙，我以食指贴着嘴唇，像要她沉默般地指示了的关係。
我为了达成某个目的，等待着知道爱丽丝是伯爵家关係者的学生毕业。
虽然不介意对作为教师的爱莎报出我的身份，但周围有穆勒学园的学生在。倒不如说由於我们吵吵闹闹的缘故，有不少学生探出脸看究竟發生了什麼事。
好不容易知道爱丽丝是伯爵家关係者的全体学生都毕业了，却在这种地方暴露身份的话，对被看到在一起的爱丽丝会有坏影响呐。
「那孩子是……那个……」
「我是利约。不久前，在我困扰的时候被丽安娜帮助了」
「是，就是那样。我帮助了利约君――呼哎哎哎！?是我帮助的吗！?」
「哎，有帮助了我的吧？」
「不，我觉是相反的说……」
「你在说什麼啊。明明是我受到丽安娜帮助的吧」
「是，是啊。我感觉好像是帮助过了」
虽然变成像是威胁着她的感觉，但事实上靠着丽安娜她们的努力而令领地的粮食问题得到解决呐。说得到了帮助并不是谎言啊。
「哎，是这样啊。所以利约君才会憧憬着丽安娜前辈呢」
「嘛，差不多就是这样？」
「呼哎哎哎！?利约――君你，憧憬着我吗！?」
丽安娜慌张过头了哦。倒不如说，连敬语也说出来了真的快给我停下来啊。
「原来如此。虽然想给你的心意应援，但丽安娜前辈憧憬着这裡的伯爵大人的关係所以难度很高的呦？」
「爱爱爱莎！?究竟在说什什什什什麼事呢！?」
「说什麼？（就）事实啊。丽安娜前辈，这样的事不从最初好好地告知的话，往後受伤的会是这孩子呦？」
「不是那样的！虽然是这样，但不是这样呦！」
「那个……到底是哪边啊？」
……姑且，爱莎是担心着我的样子呐。虽然有点大咀巴的感觉（藏不住秘密），但不像是坏孩子啊。
那麼丽安娜的事……嗯，就决定装作没听见吧。再继续让她慌张下去就太可怜了，总之先把话题岔开吧。
「丽安娜憧憬着利昂伯爵这件事还真是第一次听说啊」
「呼哎哎！? 那，那个，只是爱莎单方面的自说自话罢了！」
「什麼嘛，是骗人的么？嘛利昂伯爵他，也不是那麼值得憧憬的对像呐」
「没有那样的事！对（这样的）我非常温柔，而且特别有决断力，那样的利昂大人对我来说是――」
「……对我来说是？」
「什，什麼都没有！！」
丽安娜像是要从头喷出蒸气般的红着脸地垂下头了。
……哎？刚不是想改变话题的方向的吗？不，看到慌张的丽安娜后，稍微变得想调侃她了。
就像，被大家憧憬着的大姐姐，只对自己显现出那可爱的一面的话，不是稍微来感觉了吗？那一点像是优越感，之类的感觉呐。
说笑的。到底是（再继续下去就）太可怜了，差不多放过她了吧。
「丽安娜。抱歉叫住你了。本是和朋友一起来吃蛋糕的吧？」
「啊，对，对啊。那麼爱莎！（我们）换个座位吧！」
「哎，但是，我想在这座位和利约君多聊一会」
「好·了·啦！那麼，要移动了呦！」
「等，前辈！不要推，请不要再推了！」
「那麼利约君。我们下次再见面吧！」
就是这样，爱莎被丽安娜强行地带走，往稍微远方的座位移动了。目送（她们离开）之後，我转身回到单间中。
――的时候，和瞪着我的爱丽丝的目光重叠了。
===========================================================================================================
利约这个假名今後也会再出现。
利昂的假名是利约这点不就完全暴露了吗！ 是说，会有不少看倌这样想的吧，因为是完全不同的名字，会让人联想这傢伙是谁呢？ 因为别的作品也是这样处理，所以“对於利昂的名字那点”,就来了个直接的假名。（不就是将英文的Leon换成片假名的Leo么）
虽然有点硬来，请谅解。
