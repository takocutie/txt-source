









「那麼、诸位、就暂时告别了。都要健康度过每一天！」

奥加大师站在村子门前、和聚集起来的村民们说着告别的言辞。

「大师、又不是什麼生离死别、说的太夸张了」

「就是啊。你那句、就像喜欢历史剧的奶奶说的话似的」

奥加大师的话被奥乌卡、里奥这对情侣诟病了起来。
村子裡剩餘的奥加在200以上。今次参与讨伐的除去奥加大师、里奥、奥乌卡之外还有65名奥加。以防万一留了40名战士在村子裡、然後我们就向沉睡着贝希摩斯的森林进发了。
把65名奥加细分的话、其中有奥加大师率领的精锐部队20名。
奥乌卡及其部下共20名。
里奥他们使用棍和枪、还有弓的中远距离部队15名。
剩餘10名是搬运货物。确保食物和过夜处、等担当杂务的部队。樱也在其中。
行军的速度是快速步行的程度、樱也能毫无问题的跟上。虽然樱失去了一只手臂从此与战鬥无缘、但是因为强化了状态值、所以是不会被杂务部队的奥加们添麻烦的吧。

说起奥加、这个种族有着相当与众不同的特徵。在战鬥前为了掌握己方能力而收集了情报、想知道他们的身体能力结果发现了颇有意思的结果。
首先、说起奥加的话就是他们那巨大身躯吧。平均身高的话女性能打倒170.男性则是超过2米。身体能力上6岁的婴儿就能和成年人类匹敌了、成年的话就能有人类2到3倍的身体能力。
我由於提升了技能等级、所以身体能力上不会输给一般的奥加、但是他们的力量基本上和状态值等级升到3级了的樱是等同的。
身体特徵的话还有一点、就是头上长有角。
角的数量基本上都是两根、但是也会稀有的出现一根的还有三根以上的。
从前、一根角的奥加被视为残次品。因为生下来之後、就比别的奥加消瘦纤细、力量上也是劣于大部分奥加。
但是、自从在每年村子举办的格闘大会上、被身为转移者的奥乌卡的祖母锻炼的一根角奥加获胜之後、奥加们就对其刮目相看了。
现在在村子裡一根角都被认为是拥有潜力、大器晚成的类型。
反过来角多的奥加则是生来能力就高、角越多就拥有越多的天生技能。目前角的数量最多的是八根角的奥加。而这奥加——

「诸位、明日就会到达贝希摩斯沉睡之地了！都做好觉悟吧！」

正是站在军队之前發出号召的、奥加大师。
听到这件事之後、就盯着奥加大师的头看个不停、但是怎麼看都只是帅气的一根角、和与其相对的折断了的一根角。
提出这个疑问後、奥加大师开口大笑、背向我们之後抬起了他的头髮。

「除了这两根突出的角之外、还有这种在后脑的像疙瘩一样的小的角。不过凹凸不平的洗头很麻烦就是了！」

之前被长髮盖住了所以完全不知道、但是那裡确实有着小型疙瘩那样的幾个角在。
据说奥乌卡角也很多、总共有五根的样子。
里奥的话实际上正是一根角、他就是被奥乌卡祖母锻炼後获得了優胜的奥加。听这一说才注意到、确实他的额头中心稍微偏上的位置长着一根角。
混有身为转移者的日本人血液的话、生出一根角或者三根以上的角的可能性很高、年轻的奥加生出两根角以外的情况并不稀奇。
要说其他奥加与众不同的地方的话、就是奥加里没有掌握魔法系的或者什麼什麼操作系技能的存在吧。
生来就有的技能都是强化五感和身体系的技能、和魔法相关的技能相性很差的样子。所以说、他们在进行远距离攻击时只能使用弓和投枪了。
尽管如此、活用优异的身体能力的话、还是能够做到在这个岛上存活的、只不过在遇到奥乌卡的祖母之前、奥加还是总数不满50的小规模种族。
对於那时的事情已经被父母和爷爷奶奶唠叨到腻烦的奥乌卡、对我说明了当时的情况。

「由於当时应对精神系的能力很弱、有着只能正面作战的缺点、爷爷他们当时處於十分危险的地位。然後了、在遭到其他转移者袭击的时候祖母过来帮忙了、然後就繁荣下来了」

说起奶奶活跃的话题时、奥乌卡比说自己的事情时还要洋洋得意。
真的很喜欢奶奶的吧。

「然後呢、在施以援手之後、奶奶怒喝到、再这样下去你们都会全灭的！然後、就强迫我们学习了『气』这个技能！」

还真是大吃一惊、奥加们全员都能使用『气』这个技能。
据说、从认字之前就开始进行气的锻炼了、这在奥加的村子裡是常识一般的必修事项。
对於怎麼教都学不会魔法的奥加们来说、本身就和『气』的技能相性很好吧。奥加们觉醒了『气』的技能之後、提高了对精神系技能的耐性、同时也使身体能力更上一层楼。
奥乌卡操作『气』的能力要超过他人的样子、据说其能力比奥加大师还要强......不过也就是她自己说的罢了。
听到这个之後权藏眼睛冒光的说着「师傅、务必、请传授与我！」这样的话拜托了奥乌卡。之前好像就很觊觎我这个便利的『气』技能就是了。
奥乌卡也没否定的样子、约定了在这场战鬥之後就正式的教授权藏。
如果、权藏入手『气』这个技能的话、完全可以期待其战力的精进。而在那之後、他达到我的高度也指日可待了吧。

「绝对要在此战活跃起来、然後变得有名、这次、一定要受欢迎……」

看着他在我前面一边走着一边挨个的说着自己的慾望就不禁感觉到、那种未来貌似不会到来的样子。






有关奥加已经了解的差不多了、但是问题是对手贝希摩斯了。
全长10米、长着尖牙长着类似于牛的面部的生有两根巨大角的东西。
然後、身体还覆盖着如铠甲般的皮肤、其强度则是拿着钢铁制武器的奥加用怪力击打上去、也难以留下伤口的坚硬。
尾巴则是巨大且坚硬、前端露出了尖刺般的骨头、必须要注意其甩尾攻击。

还有就是最需要注意的地方、贝希摩斯能操作土这一点。
引发地震、或从地面突起枪一般的岩石、有时还会将岩石覆在体表提高防御力。
光听说了它不会吐火、会用身体进行攻击什麼的、但是看来对於奥加来说贝希摩斯会操作土就像常识一样所以忘记跟我说明了......如果就这麼蒙在鼓里去战鬥的话、还真不知道会变成怎样。
听到这个的格鲁霍、在人工草皮中發出了郁闷的嘟囔声。

「没我出场了」

毕竟贝希摩斯的能力是土魔法或者土操作了吧。而且等级肯定相当高。也就是说、战鬥中基本没有格鲁霍的出场了。即使挖出落穴、对方也会立刻填满、土系的陷阱全都会成为摆设。
依靠手头的道具还有奥加们的实力、再加上同伴们的活跃、怎样把受害程度控制在最小而获得胜利呢。这是一个问题。

而且、也必须要注意到这场战鬥的赢家这件事。
如果使用陷阱然後再施以攻击的话、参加者都会入手经验值、但是果然完成最後一击的会获得大量的经验值吧。
经过事先的协商、完成最後一击是奥乌卡的任务。
让奥加大师来打倒、好增加实力。这种意见也提出过、但是被本人立即否定了。

「我的力量已经没有增长的空间了。就算提升了等级、成长的也微乎其微。那就不如让今後还会更加成长的奥乌卡来打倒、让她实力接近於我的话与兽人帝一战也会轻鬆吧」

通过奥加大师的决定之後、就变成了让奥乌卡完成最後一击。
不过、这是如果那时情况尚有富裕的事了、实际上任谁都有可能完成最後一击的吧。虽然自己也有坐收渔翁之利的想法、但是考虑到今後的良好关係、还是不要节外生枝、就让奥乌卡来完成是最好的吧。

「说起来土屋先生啊。有考虑什麼策略的吧？」

一边摆出考虑着什麼的样子一边让视线尾随着奥乌卡的权藏、在我旁边走着。

「到昨天为止还算是有呢、但是最後关头得知对方会操作土这件事、所以正在重新考虑来着」

用最原始的单纯陷阱、让後在下面布置好削尖的圆木和枪。虽然之前是这麼想的、但是这点不仅要消耗格鲁霍大量的精神力、而且面对会操作土的对手也只是徒劳吧。
後来又想到往落穴中灌水这个方法、但是贝希摩斯肯定会操作土做出渠之类的东西引开水的吧。
无法使用土系陷阱这点、对於室外作战十分的不利。就算我架设好我的线、这种巨大身躯也很坚固的吧。估计连阻止对方脚步都做不到。

奥加们好像打算是正面作战粉碎之、但是那必须要做好出现大量死伤的準备了。老实说还真是难办啊。
能操作土、身体坚固。感觉净是和一些棘手的傢伙作战了。
尽管如此、还是有想出幾个无法保证实效的手段。但是十分担憂胆大无畏喜欢正面作战的奥加们会不会认同就是了。
我们到底也只是帮把手的立场。不能给予对方这种很爱凑热闹的印象吧。

「里奥。有关贝希摩斯的现状的一些事能告诉我么？」

大声想附近的裡奥提出询问後。对方就快步过来。
虽然在与奥乌卡比试之後有瞪过我、但是现在关係很好了。
里奥好像是领悟到了那场战鬥的意义、战鬥结束後没多久就对我表示了感谢。

「最近就隐约觉得奥乌卡自大起来。但是、没能做到什麼。之後、她的练习已经不再偷懒了。这都是阁下的功劳、感谢」

那之後、他都能和我普通的接触了、或者说甚至打算帮忙恢復我和奥乌卡的关係。
关係变成这样了的裡奥、这次也很快回答了我的问题。

「还在持续沉睡着的贝希摩斯用坚固的岩盘覆盖着自己的身体。虽然奥加大师的话、施以一击就可以击碎岩盘、但是那之後贝希摩斯肯定会醒来的吧。如果不出手的话、还会持续沉睡数周」

据说过去贝希摩斯也曾如此疗伤过、那时就是靠其身上岩盘的颜色来计算它完全恢復出来的时机的。
因为不管弄出多大的声音、只要不破坏岩盘就不会醒来、那麼在到达现场之後可以做各种各样的事了。

「是么、谢谢啊。状况有什麼变化的话就请告诉我」

「明白了」

将视线从转身离去的裡奥那裡移开、我继续开始了脑内的设想。






在森林度过一晚、早晨我们继续向贝希摩斯沉睡之处进发。据说还有一小时左右的路程就能抵达了。
出發之前得到了侦查情报、对方还是保持着被岩盘包裹的状态沉睡着的样子。
如果此次作战演变为战鬥的话、就基本没有我的出场了吧。虽然用斧子多少能弄出一些伤口、但是要贯穿其皮肤、果然只能靠奥加大师和奥乌卡的怪力了。
而且、实际作战起来所感知到的和之前得到的情报量是完全不同的。所以不仅要考虑策略、也要在实战中时刻保持随机应变。
就在我考虑着这些事情的时候、已经步入到北之森了的样子、周围都是长的很高的大树了。

「久违了的北之森了呢」

「啊啊、是啊」

看到露出怀念表情眯起了眼睛的权藏、我也简洁的搭了话。
明明距离上次来还不算太久、但是森林中已经比过去更加的能感受生命的存在了。如果没有人捣乱的话、这片森林会成长为茂盛的森林吧。
虽然想悠闲的做着森林浴、但是现在不是做那个的时候。现在要将注意力集中于贝希摩斯。
因为已经相当接近贝希摩斯了、所以杂务部队就在此待机了。看到樱露出了不安的表情我就说到「没关係的。而且目前、还不会演变为战鬥的情况吧」。

「请一定要注意自己的身体。红先生」

感觉她真的想一起参与作战、但是知道自己的能力什麼都做不到而难受不已的樱、就这麼露出像是要哭出来的表情紧紧地抓着我的手。

「樱就等在这裡吧、大家都会加油的」

一边说着我一边把视线转向旁边。那裡是不停点着头的格鲁霍和萨乌瓦。然後还有竖起了大拇指的权藏。

「恩、我会準备好美味的饭菜等着——」

「快跑啊啊啊啊啊啊啊啊！贝希摩斯来了！」

盖住了樱的声音的是、一边喊着一边拼命跑着的侦察兵、还有在其身後一边發出地动山摇的声音一边迫近的——贝希摩斯。

