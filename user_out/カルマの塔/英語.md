# English

[TOC]

## web000/002 從崩溃的楽園中誕生的怪物

### 'ku'

- '\n\n「Ku，'

### 'hahaha'

- 'hahaha。真的'


## web000/005 『威廉』

### 'ho'

- '』\n『Ho，這還'
- '』\n『Ho～，重'


## web000/006 三級市民威廉利維乌斯

### 'tm'

- '\n「你TM幹啥！'

### 'ga'

- '\n\n「Ga，'

### 'ha'

- 'Ha！？」'


## web000/007 卡爾

### 'no.1'

- '店裡的No.1。\n\n'


## web000/016 狂躁的戦場

### 'laoziwo'

- '我很強LAOZIWOㄌㄠㄗ'


## web000/017 黒発少女VS黒羊

### 'gunter'

- 'ター：Gunter）\n\n'


## web000/026 在夜営地

### 'eckart'

- 'カルトEckart）百人'


## web000/030 艾哈徳・冯・阿爾卡迪亞

### 'gardner'

- 'ナー/Gardner）。希'

### 'erhard'

- 'ルト/Erhard）\n\n'


## web000/031 騎士與市民

### 'yes'

- '答案是YES，殿下'


## web000/032 阿爾卡迪亞的幼狮们

### 'kluger'

- 'ガー/Kluger）。要'

### 'gregor'

- 'ール/Gregor）。馮'

### 'tunder'

- 'ダー/Tunder）。由'

### 'gilbert'

- 'ルト/Gilbert）。馮'

### 'oswald'

- 'ルト/Oswald）。家'


## web000/033 居高臨下的人们

### 'claudia'

- 'ィア/Claudia）。這'

### 'eleonora'

- 'ーラ/Eleonora），克'


## web000/034 看見仇人

### 'ddeeeeeee'

- '、拉德ddeeeEEEE─」\n'


## web000/049 瞬息万変的戦場

### 'o-k'

- '\n\n「O-K。開始'


## web051/062 黒之佣兵団副団長妮卡

### 'gooouuuu'

- '不夠不Gooouuuu！」\n'

### 'zaa'

- '\n\n「Zaa！？」'

### 'muu'

- '\n\n「Muu！？」'


## web051/063 単手的武人

### 'no'

- '答案是NO。\n\n'


## web051/075 絶望的閉幕

### 'beautiful'

- '亮啊。Beautiful啊，你'


## web051/077 在奥斯瓦爾徳的宅邸

### 'schwert'

- 'ルト/Schwert），剛'

### 'beatrix'

- 'クス/Beatrix）」\n'


## web051/078 新星来臨，阻碍它的是巨星

### 'el cid campeador'

- '班牙語El Cid Campeador」，指'

### 'el cid'

- '稱他為El Cid，阿拉'

### 'campeador'

- '尊稱；Campeador意為「'


## web051/079 停滞的白仮面

### 'jian'

- '。把這Jian（賤）'


## web051/080 联系（CONNECTION）

### 'konrad'

- 'ート/Konrad）軍團'

### 'anneliese'

- 'ーゼ/Anneliese）大人'


## web051/085 王與騎士

### 'eduard'

- 'ルト/Eduard）的代'


## web051/087 暗中活躍的英杰

### 'bernhard'

- 'ルト/Bernhard）。馮'


## web051/090 王者君臨

### 'main gauche'

- '短劍（Main Gauche）。這'


## web051/092 復仇開戦

### 'victoria'

- 'リア/Victoria）。還'

### 'rnbach'

- 'Rnbach）。今'

### 'wilhelmina'

- 'ーナ/Wilhelmina）已經'

### 'theresia'

- 'ジア/Theresia）姐姐'


## web051/093 最强的弱者

### 'grevillius'

- 'ウス/Grevillius）」\n'

### 'sven'

- 'ェン/Sven）啊，'

### 'connection'

- '聯繫（Connection）。\n'


## web051/094 維多利亞

### 'by'

- '刷的，By杉下右'


## web051/095 因果巡回

### 'bridget'

- 'ット／Bridget）。雷'


## web051/098 最大戦力集結

### 'dux'

- '大將（Dux）的地'

### 'marcellin・re・valo'

- '瓦羅（Marcellin・Re・Valo）所率'

### 'jacqueline・la・bourdarias'

- '亞斯（Jacqueline・La・Bourdarias）。他'


## web101/103 天灾

### 'celestin'

- 'タン／Celestin）」\n'


## web101/105 神之劫火

### 'it is show time'

- '\n\n「It Is Show Time！」\n'


## web101/108 胎動的新星１

### 'vortigern'

- 'ガン／Vortigern）」\n'


## web101/109 胎動的新星２

### 'lohengrin'

- 'リン／Lohengrin）。是'

### 'euphemia'

- 'ミア／Euphemia）」\n'

### 'medraut'

- 'ウト／Medraut），作'

### 'pellinore'

- 'ノア／Pellinore）。在'

### 'tristram'

- 'ラム／Tristram），留'


## web101/112 艾因哈爾特

### 'candelario'

- 'リオ／Candelario）所毀'


## web101/113 創意的天才

### 'eckart'

- 'ルト／Eckart）閣下'


## web101/118 突来的招待状

### 'gardner'

- '德納（Gardner）啊）'


## web101/123 强襲×撃退×陥国

### 'vilnius'

- 'ュス／Vilnius）。作'


## web101/139 奮戦的幼狮

### 'main gauche'

- '短劍（Main Gauche），男'

### 'breaker'

- '短劍（Breaker）（注'


## web101/142 介入的王

### 'hauteclere'

- '弗佩劍Hauteclere，『高'


## web151/152 苍盾與白剣

### 'soprano'

- '高音（Soprano），那'


## web151/169 紅莲降臨

### 'failnaught'

- '印弓（Failnaught）」\n'


## web201/214 淘金熱：闇の王国１

### 'stradiotti'

- 'ィス／Stradiotti：東羅'
- '方軍（Stradiotti）\n*'
- '羅馬的Stradiotti為重裝'

### 'scutum・testudo'

- '：Scutum・Testudo：步兵'

### 'metagame'

- '：Metagame：不專'


## web201/233 王会议：军棋１

### 'cisalpina'

- '皮那（Cisalpina：正式'
