亲卫队的搜寻已经接近两个小时，虽然是很任性的话。

但是我希望他们能更快一些。

无论是作为第一王女，还是作为姐姐的立场。

“陛下，如果可以希望能派妾身亲自去寻找。”

我单膝跪地，现在的我，是作为一名骑士向国王请求。

“不必，你太显眼了，会把事情闹大的。而且情况还没有严重到需要派自己的女兒执行这样的任务。要记住，在身为骑士的同时，你还是第一王位继承人。”

父王的语气非常坚决，就像在责备我的不成熟一样。

“十分抱歉，说出了如此任性的请求。

“不用道歉，作为一个父亲的立场，我也十分担心。”

虽然父王那冷静的声音从未改变过，但是我注意到他的眉头也皱了起来，想必也是很焦急地等待亲卫队的消息吧。

“安心吧，赫蒂，一定很快就能找到的，你父王和我也是一样的心情。”

守在父王身边的母亲也安抚了我。

………………

你究竟要多麼被大家疼爱到什麼地步啊，我可爱的妹妹。

克莉斯是一个被神所宠爱的孩子。

我三岁时亲眼看着母亲的生产。

王宫的大家都夸她很可爱。

这不单单只是奉承之言。

随着逐渐长大，她的头髮渐渐长了出来。

那是一头仿佛初雪般的美丽银髮，加上那双犹如石榴石般透明的红瞳，白瓷般的滑嫩柔肌。不过五岁时她已经是众所周知的美人胚子。

周边王族的求姻之言更是不绝入耳。

见过克莉斯的贵族甚至都异口同声地称讚她犹如百年前的弗莉亚女王再世。

我的容貌虽然还没到会让父母丢脸的地步。但和妹妹一起便是相形见绌，每次出席宴会幾乎大家都把目光投向她，甚至有人会私底下说是不是由克莉斯继承王位更好。

但是，我并不会怨恨这些人。

因为光是那银髮红瞳就有着那般勾魂摄魄的魅力！

连我自己有时也不禁会看入迷。

年幼的时候我嫉妒着克莉斯，嫉妒她的美貌，嫉妒大家都那麼关心她。明明我才是第一王位继承人！你们这些傢伙对未来的国君就是这样的态度吗？

於是我便拼了命地练习剑术，礼法，骑术，诗歌。想成为一个让人无法挑剔的骑士女王。以英明的领导力和优雅的风度让大家信服。

相反，克莉斯却除了礼法外对其余的课程完全不上心，天天就是乱跑到王宫的各处冒险，光是寻找就让大家费了不少心。

（哼，这样的你也迟早会被大家说成只是一个花瓶而已吧。终究能治理国家的不是天生的美貌，而是後天的努力。）

可是，事实真的是这样吗？

逐渐地，我注意到了一些大家没在意的地方。

一开始，克莉斯只是无目的的到处乱跑。但是後来，她却不再乱跑，每次都是翘了课程跑去图书馆。

在图书馆能幹什麼呢？大家都不怎麼在意，不喜欢学习的孩子会去图书馆看书？怎麼说也太奇怪了吧。大概只是找个没人的地方躲着睡午觉吧。

而且确实每次被发现时克莉斯确实什麼都没干，拍了拍身上的灰尘就老老实实地跟着仆人回去了。

王家图书馆大得让人难以想象，整整3层排满了连两个成年男人也够不到顶的书架。

并且不会允许一般人随意进入，躲在这裡也很难被找到吧。

大家理所当然地认为不过是妹妹偷懒的诡计而已。

但是为什麼要一直躲在同一个地方呢？这个除了书没有半点意思的图书馆裡。

疑惑在我的心裡逐渐升起，到了在意的不得了的地步。

有一天我偷偷装病跟踪了妹妹。

她巧妙地骗过了护卫，独自一人穿过了花园，绕过了大厅，其娴熟的脚步让我连跟上都十分费力。终於，我在没被她发现的情况下成功潜入了图书馆。

结果让我无法相信，那孩子一直在阅读一些我根本弄不明白的书。

好像关於大陆的歷史，地理以及文化的各类书籍。

并且常常带着笔记记录一些仿佛很重要的内容。

这孩子一直逃了课在这裡研究这些吗？为什麼要看这些东西？

那天，克莉斯比往常更早地走出了图书馆。

她显得异常兴奋，蹦蹦跳跳地一人走向花园。

她究竟要幹什麼？

我也带着疑问追了上去。

很快，我知道了答案。

“温蒂尼，请施予妾身淨化之水。”

克莉斯一手拿着笔记，闭上眼睛，嘴裡念念有词。

难道是要施法？

怎麼可能，往往10个人裡只有一个人有魔法能力。

其中男女比是2:3，虽然王家女性一向具有较高的魔法天赋，比如我在圣骑士团裡接受过训练可以使用一点点神圣魔法，但即使如此也是在两个星期的练习中才能稍微使出啊。

没有接受任何训练的她怎麼可能……

事实击碎了我所想的。

空气中出现了透明的水球。

并且不断凝结，越来越大。

“希瓦，冻结！”

水球化成冰球。

怎麼可能啊，怎麼可能嘛！

第一次练习就完美地同时施展两个法术吗？

她真的是第一次使用吗？

不会错的，以往都是被大家从图书馆带回来的，她总不可能在那裡施法吧。

你究竟是什麼样的怪物啊！

难道说在头脑上神明也这样眷顾於她吗？

克莉斯是个天才，无师自通。

你究竟要完美到什麼地步啊！你就是为了嘲讽我才出生在这个世界上吗？

过於震惊的我脚底一软，踢到了周边的石子。

（糟糕！）

“欸？”

克莉斯也非常惊讶地转过头来。

“赫蒂姐姐大人！”

“唔，你好啊，克莉斯。”

“姐姐大人在这干……幹什麼呢。”

“啊哈哈，我来练剑的。”

我到底找了个多麼愚蠢的理由，我根本就没有带剑！

“这样啊，啊哈哈~”

看来她也一时反应不过来。

“那麼，我先走了。”

我慌忙打算逃走。

“欸！等一下，赫蒂姐姐！”

才不等，我一刻也不想在这裡停留，幸好我体力要比克莉斯要强很多。

看到没希望的她也并没有追上来。

那麼我该怎麼办？就这样尴尬地跑掉了……

之後又要怎麼跟克莉斯打招呼呢。

…………

……

回到房间的时候我已经什麼都不想做了。

女仆们送来的粥也完全不想吃，本来只是跟踪妹妹的借口而已……但现在的我好像真的病了，倒在床上什麼都不想做，无论是身体还是精神都充满了无力感。

叩叩叩……

敲门声响起在耳边。

是谁啊，不要来打扰我啊。

“姐姐大人，是我哟。开下门吧，我带来了很好吃的蛋糕哦~”

偏偏是你。

没办法，面对妹妹是不能任性的，必须有姐姐的气量才行。

我过去打开了门。

“有什麼事吗？克莉斯”

我尽量让自己的声音和平时一样，因为现在仅仅是面对她我的心情就会变得十分糟糕，尽管这只是醜陋的嫉妒，但是我无法让自己更加平和一些。

“非常抱歉，赫蒂姐姐大人，非常冷呢。进去说话好吗？”

克莉斯微微一笑，身上只穿着薄薄的丝绸睡裙，手上端着茶点的托盘。

她是这样狡猾，让我没法拒绝。

“进来吧，不过可不能太晚哦。”

我摆出姐姐的架子。

“嗯~~这个起司蛋糕是凯瑟琳做的哦。”

唔，色泽很漂亮的小块蛋糕上点缀着一颗蓝莓，看上去确实非常好吃。

於是我们便坐下来一起享用蛋糕和红茶。

“姐姐大人，你把下午那件事告诉了父王母后了吗？”

“还没有哦，当时仅仅是太吃惊了而已。”

“请千万拜托不要把我偷偷练习魔法的事情告诉任何人！”

克莉斯露出请求的表情。

“为什麼呢？”

“欸？”

“为什麼要隐瞒呢？这是很了不起的才能哦，父王母后知道也会很高兴的，大家都会认为克莉斯是天才啊。”

我不理解的就是这点，我拼了命去让别人确认我的价值，但是克莉斯却不想让别人知道。

“嗯，非要说的话，我不想做这麼显眼的事情，很困扰。”

“这……这算什麼理由！”

“姐……姐。”

“虽然很不甘心，但是我一直都羡慕你哦，拥有那样的美貌，还这麼聪明，太不公平了吧。”

“…………”

克莉斯她低下头不说话，我太激动了，没有控制好自己的情绪，姐姐失格啊。

“对……不起”

“不，我才是。明明克莉斯什麼都没有做错。”

“但是我没觉得姐姐大人有哪裡比我差呢。”

安慰我吗？

“姐姐大人的金髮和蓝眼我感觉要更漂亮，如果没有弗莉亚的传说的话，大家都会说我们有着相同的美貌才对。”

“是吗？”

“绝对没错哟！我们可是同一个母亲生下来的姐妹哦，姐姐大人将来一定会和母亲大人一样美丽。而且关於魔法我不过是练习了书上写的最基础法术而已。才11岁就能进圣骑士团的姐姐大人才更加耀眼呢。”

“唔……那是因为我天天练习剑术。但是你，是一个天才。”

“才不是呢！”

“欸？”

“我也是还不容易在图书馆思考了两个星期才摸清楚魔法的使用方法哦。因为魔法真的是很困的东西……我是一个理论派，所以一直看了很久的书发现了一点诀窍才敢练习。”

“是，这样吗……看来我有些误会了，甚至嫉妒你，真的是太难看了。”

“不是这样哦，我一直觉得能这麼努力的姐姐大人非……非常的了不起！我一直以来都做不到这一点。做什麼都是三分钟热度，结果最後什麼都做不到……我不想再这样下去了，赫蒂是我骄傲的姐姐……让，让我们好好相处各补所长吧，姐姐大人。”

克莉斯的声音变得断断续续的，在月光的照射下能看见她眼角的泪光。

对着这样的妹妹，我动摇了。

也许不需要老师的克莉斯确实是有才能。但是，不管如何，擅自否认别人的努力是弱者的行为，这样的我究竟有多麼醜恶。无论怎麼想，都无法逃出自我厌恶的漩涡。

即使这样，她也愿意主动敲我的房门，和我说这番推心置腹的话……想和我和好。

我究竟要差劲到什麼地步。

“呜呜……我也想和克莉斯变得要好。”

最终我也不争气地哭了出来，而克莉斯将大哭着的我拥入怀中……

那一天我们相拥入眠了，这样的她，或许比我更适合担当姐姐的角色。

直到现在我也不会改变自己的想法。

没错，克莉斯是一个被神宠爱的孩子，大家都喜欢她，无论闯多少次祸都能被原谅。我嫉妒这样的她！

但是，也和那份嫉妒的程度一样地爱着她。