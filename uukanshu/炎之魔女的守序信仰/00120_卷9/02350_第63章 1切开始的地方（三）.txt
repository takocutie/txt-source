法莱恩.西格.莎法尔。

西格是莎法尔伯爵父亲的名字。

这也代表着法莱恩少爷有着第一顺位的继承权。

我本来是这麼以为的，但是他却有着自立门户的想法。

现在年值22岁，没有交往中的女友。

长着俊朗的外形和一头清爽的微卷髮。

人也很开朗，健谈而且知道～分寸……即使在家裡也保持着优雅的坐姿一直面带微笑。

比起少爷来说要成熟的多，据说他还是用剑的好手，擅长冰属性的魔法。

仅有这些的话就能成为名门大小姐们投怀送抱的对象了。

但是，还不止呢……

再以相亲中贪心女孩的心态来说说这位男士的工作吧。

目前正任职迪尔兰王都的公职，表面是这样说的……

明明是骑士出身却挤入文职，并且提出了幾个方案和平解决了国内幾位领主长年欠税的问题。好像国王陛下正有提拔他的想法，并且封了法莱恩少爷新的领地和爵位。

这已经是可以被记入本国年代记的功绩了吧……

但是没有一种实感。

突然告诉我，“啊，一个英雄站在你的面前哟~~他有可能是你将来的哥哥哟。”

你能够一脸笑容地点头吗？

我首先感觉的就是没有真实感。

这麼完美的男人是闹哪样啊，还单身？

你以为你是少女漫画的男主角等着女主角攻略吗？

如果说这些只是个人的主观意识的话，我对他没什麼好感的原因还有一个。

浮现在他的脸上的让人毛骨悚然的笑容。

眯着眼睛。嘴角的弧度微微向上。

明明才第一次见到法莱恩少爷，但是却感觉在哪裡见过一样。

但是并不是什麼好的记忆……也许是我个人的感觉太重了吧。

毕竟他也是少爷的哥哥。

“初次见面哟。小姐~~还有这位同样美丽端庄的少女是？”

“希尔维娅……”

希儿敷衍地回答着，看上去对法莱恩也并不感兴趣。

“希尔维娅小姐是吗？真是吓了一跳呢。这名字简直和帝国帕拉狄斯的皇女一样……而且您的美貌也和她很相似呢~~”

这傢伙！已经看出来了吗？

这一回答就连希儿也一时之间仿佛流出了冷汗。

“法莱恩少爷……希儿她……”

“那麼，小艾文~~谁才是你的挚爱呢？”

我的话被他一下打断了并提出了新的话题，是并不在意希儿身份吗？还是不想在我们面前深究这个问题？

不过接下来的话题，少爷也不得不认真地面对呢。

将我们逃亡的事情，同时和我和希儿两人交往的事情告诉莎法尔老爷和夫人……

我看了一下少爷，他对我们两人点了一下头。

“父亲，母亲……大哥，我有话想对你们说……”

“嗯？怎麼了……”

“哎呀哎呀，怎麼一副这麼认真的态度？难道是要和我们商量準备和小克莉斯结婚的事情吗~~还是说是那边的希尔维娅小姐呢？”

夫人开着玩笑揶揄着少爷。

这对她来说已经有着微微责备的意思了。

“唔……虽然也并不是不沾边来着。但是我们希望把所有事情告诉大家。”

所有事，之前我们已经商量好了。

被追杀，不会再回学院。

可能再次走上旅途。

尽管不是很体面的事情，但是必须把这些告诉大家。

不仅仅是老爷和夫人。

也包括艾露和梅阿莉姐……琳达，尤朵姐。

夏尔年纪太小就算了。

至于法莱恩少爷，他颇有兴趣地坐在沙发上。

其实我们最不想涉及到的人大概就是他了，不仅是希儿，对我而言也像个陌生人。

少爷也没怎麼说过他的好话。

不过毕竟也是艾文的哥哥，不能单独排除在外。

“莎法尔老爷。夫人……妾身也是，有想要告诉你们的事情。”

少爷站起身，走到了大厅的正中央。

我拉着希儿跟在身後……

…………

花了大概两个小时，我们叙述了很多事情。

首先是我的真实身份。希尔芙利亚的第二王女……

之後成为了一个魔女来到了特罗洛普。

後来就是大家知道的事情了。

所以和他们所说的故事从我离开了莎法尔家开始遇到了魔女之夜开始。

遇到了魔女的同伴们，不过这一段我把她们的下落省去了，把古代人的部分也省去了。

学院开始换成少爷来说。

与冰龙一族结怨。

希儿的身份。以及那两人的羁绊……

他们的一些事情我也是第一次听到。

到最後我们形成了这样微妙的关係。

以及少爷下定决心的事情。

莎法尔老爷是知道一些真相的，所以除了少爷那句要和我与希儿两人交往并且结婚之外并没有受到什麼冲击。

夫人则是有些惊讶地掩着自己的嘴巴。

梅阿莉和艾露姐则是一副不耐烦撑着下巴。尤朵边流着泪边想过来拥抱我。

琳达则是大眼瞪小眼难以置信。

但是只有那傢伙……

法莱恩少爷，他的样子没有产生一丝一毫的变化。

依然是那种让人毛骨悚然的可怕笑容。

大概就是这副笑容让他单身到现在的吧……

那只能让女生感觉到危险呢。

说完之後我们并没有立刻请求大家给与回应。

这种连番真相的轰炸大概谁都不是一下能接受的吧。

於是大家都按着自己的头要求先休息一晚明天大家再聚在一起商量。

当晚我们全部被分在了不同的房间。

这也是要让大家各自思考一下的意思吗？

毕竟一下子接受了这麼多信息。

对於我的印象各自也有了不同的看法了吧。

老爷知道我身为王女的身份。但是对於魔女的方面我却从未暴露。

其他人又会怎样呢。

琳达那样一丝不苟的女性会彻底否定我吧。

梅阿莉和艾露姐……好像不关心这类事，更在意少爷和我以及希儿的三角关係这一点。

尤朵姐看上去倒是毫不关心我的身份的事情。但是是否会对这些事情感到不安了呢？

叩叩叩……

敲门声响起，是少爷来夜袭了？

不过出现在眼前的却是尤朵。

“尤朵姐~~睡不着吗？”

“克莉斯刚才露出了好失望的表情啊。不想见到我……吗？”

没过一会尤朵又变成眼睛湿润梨花带雨的样子。

“不不……没那种事！”

“那麼我进去了哟~~”

“恩，请进。”

我把床的位置空给了她。自己坐在椅子那边。

我们之间的空气凝固着，不知道该先说点什麼。

真是的，都是女孩子在意些什麼啊！

“尤朵姐……好久不见了。”

“嗯，虽然才过一年而已哦……但是总觉得比三年都久哟，克莉斯好像吃了不少苦呢。”

“确实是那样是没错……”

但是那并不是很讨厌的事情。

不如说让我明白了人生的诸多……

“之後克莉斯还打算出去旅行吗？不是已经无法回到学院去了吗……”

“的确是这样，但是……”

“留在这裡吧，克莉斯……成为艾文少爷的妻子之後你也能过上无憂无虑的生活哟，更何况你本身就喜欢他呢，虽然艾文少爷好像有点花心了……但是我觉得他从内心深处真正喜欢的是你哟。虽然这样说很失礼。但是对那个女孩子，希尔维娅殿下。少爷可能带着些歉意的成分吧。”

尤朵半跪在地上，伸出那有些起茧子的手掌温柔抚摸着我的脸颊，如同真挚地向我请求一般。

我现在已经不是那麼天真的，刚来异世界对什麼都好奇的转生者了。

这个世界有着各种各样的危险，阴谋和邪恶……

秩序的力量显得尤为薄弱。

成为一个领主的妻子过着无憂无虑的生活无疑是最为快乐的事情了。

更何况少爷根本就不在乎我魔女的身份。

一直能保持年轻漂亮的样子也不会让他变心吧。

但是……

这不是我真正想追求的事物。

每个人都拥有着的小小的梦想……

我也有着一个。

“妾身……没考虑过这些。”

“……”

“这些事情，让妾身觉得害怕，孤单，无趣。迷惘，踌躇。”

我的心情在不断变化着。

最近让我心动的事情又多出来了一件。

坐在回特罗洛普的的马车上时，我和希儿以及艾文少爷三人依靠在一起时的那份心情绝不是虚假。

只有三人在一起冒险时我们才能维持住真正的自己。

没有一点点隐藏自己的外壳。

其实，尤朵刚刚的发言可能让我有些生气了。

“而且你刚才说的不对哟……”

“不对？”

正是因为不了解才容易被表面所欺骗。

正是因为了解才知道他真正的想法。

“嗯。如果把心臟天平的两边上……向妾身的这边倾斜的幅度……应该稍微小一点吧。”

“…………”

我微笑着看着尤朵的眼眸。

额头抵在一起，近到能感受到彼此的呼吸。

“尤朵姐还是把妾身当做孩子来看吗？”

她轻轻摇了一下头。

“不，你已经是出色的大人了。”

是吗……

我的眼眶也跟着尤朵姐一起变得湿润了起来。

她已经18岁了。

在原本的世界的祖国也算是成年了呢。

在这个阿尔克纳拉的世界裡早已经是该为人-妻的年龄。

但是尤朵她已经或多或少承担了母亲的职责了吧。

夏尔。不能太让姐姐担心哟。

虽然也许我没资格说这句话呢……(未完待续。如果您喜欢这部作品，欢迎您来起点（）投推荐票、月票。您的支持，就是我最大的动力……)